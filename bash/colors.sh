
#!/bin/bash

#######################################
# generate the main Archi site from
# rte_main_model git repo
#
#
######################################

source $(dirname $0)/parameters.sh


echo "#################################################"
echo "#################################################"
echo -e "# ${BPurple}Suivi quotidien serveur ${NC}"
echo "#################################################"
echo "#################################################"

echo -e "${YEL}Vérifier les logs d'accès au serveur ${ORA}"
sudo cat /var/log/secure | grep  "Accepted password" | tail -n 50
echo -e "${YEL}Vérifier les erreurs d'accès au serveur ${RED}"
sudo cat /var/log/secure | grep  "Failed password" | tail -n 50

echo -e "${YEL}Vérifier les logs d'accès Apache${NC}"
sudo cat /var/log/httpd/access_log | sed -e 's/^\([[:digit:]\.]*\).*"\(.*\)"$/\1 \2/' | sort -n | uniq -c | sort -nr | head -20

echo -e "${YEL}Affichage des erreurs apache ${RED}"
sudo cat /var/log/httpd/error_log

for siteName in "${SITES[@]}"
do
        /applis/archi/noumea/check_daily_by_site.sh $siteName
done


echo -e "${NC}---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

