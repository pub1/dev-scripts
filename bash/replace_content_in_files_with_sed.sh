#!/bin/bash
SITE_NAME=$1

#######################################
# script that uses sed to replace some href="url" by others urls from some other files
#
######################################

echo -e "specific post processing $SITE_NAME in  /var/tmp/website_$SITE_NAME"

cd  /var/tmp/website_$SITE_NAME
# in the first set of files :
# grep all files that contains a hyperlink between >< (and only a hyperlink)
# write the ouput to file
# each line will look like :
# /var/www/html/processdi/id-0971ffb01b424396b524965ac79873a4/elements/id-2643e69165434b2eb77aa7a5ec9859b7.html:>https://toto.com
grep -o ">https\?://[^<]*" /var/www/html/processdi/*/elements/id*.html  > /tmp/processdi.tmp

# read the tmp file
cat /tmp/processdi.tmp | while read line 
do
	# get the first token (everything before >  and clean it
	# /var/www/html/processdi/id-0971ffb01b424396b524965ac79873a4/elements/id-2643e69165434b2eb77aa7a5ec9859b7.html:>https://toto.com
	# we will get
	# awk :  /var/www/html/processdi/id-0971ffb01b424396b524965ac79873a4/elements/id-2643e69165434b2eb77aa7a5ec9859b7.html:
	# sed : /elements/id-2643e69165434b2eb77aa7a5ec9859b7.html:
	# second sed : /elements/id-2643e69165434b2eb77aa7a5ec9859b7.html
	ELEMENT=`echo $line | awk -F">" '{print $1}' | sed 's|.*\/elements\/|\/elements\/|' | sed 's|:||'`
	# add .. before the path of the element as href in views html are relative
	ELEMENT="..$ELEMENT"
	# get the second token which is the target external url
	# here : https://toto.com
	URL=`echo $line | awk -F">" '{print $2}'`
	# display what we are going to do
	echo $ELEMENT "should be replaced by" $URL
	
	#In the second set of files
	# replace every link to $ELEMENT with $URL 
	#find filter on html in views folder only (I do not want to break other things in the website) 
	#sed :
        # -i inline
        # -r extended regexp (and not recursive :-)) 
	# (.*)$ELEMENT(.*) : first parenthesis is everything before $ELEMENT , second is everything after
	# |\1$URL\" target=\"_blank\">| : \1 referts to first parenthesis in previous expression, \2 to second parenthesis
	# here we tell sed to
	# keep everything before $ELEMENT,
	# replace $ELEMENT by $URL 
	#and then  replace the rest of the line with \" target=\"_blank\"> (we discard \2)
	# the lines we are looking for looks like
	# <area ..... href="../elements/id-2643e69165434b2eb77aa7a5ec9859b7.html" target="_frame_name"/>
	# what we want is : 
	# <area ..... href="https://toto.com" target="_blank"/>
	find . -type f -wholename "*views/*.html" -exec sed -i -r  "s|(.*)$ELEMENT(.*)|\1$URL\" target=\"_blank\">|" {} +

done

 
echo "end of specific post processing"


