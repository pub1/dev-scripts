#!/bin/bash
##########################################
#sample installation script
##########################################
if [[ $USER != "archi" ]]; then
    >&2 echo "script must be run as archi user"
    exit 1
fi



if [ $# -eq 3 ]; then
    >&2 echo "need 3 parameters"
    exit 1
fi



##########################################
# folders creation
##########################################

sudo install -d -m 0755 -o archi -g archigrp /apps/data/archi 



##########################################
#get artefacts from nexus server
##########################################

curl -v --user $USERGAIA:$PASSWORD --output /tmp/SRV_INST/noumea-bin-0.1.0.tar.gz https://some_url/bin-0.1.0.tar.gz


cd /tmp/SRV_INST
tar -xzf bin-0.1.0.tar.gz -C  /tmp/SRV_INST


##########################################
# misc
##########################################
cd /tmp/SRV_INST/bin
sudo cp segoe/*.* /usr/share/fonts/segoe
sudo fc-cache -vf
sudo cp -r html/* /var/www/html
sudo chown  -R archi:archigrp /var/www/html/assets

##########################################
# restart server
##########################################

sudo systemctl  restart httpd

##########################################
# yum
##########################################

sudo yum -y install gtk3.x86_64 xorg-x11-xauth mesa-libGL xorg-x11-server-Xvfb.x86_64 mutt

##########################################
# crontab
##########################################

crontab -l | { cat; echo "0 6 * * * /apps/data/archi generate_site_job.sh > /var/log/archi/generate_site.log 2>&1"; } | crontab -


echo "end"
