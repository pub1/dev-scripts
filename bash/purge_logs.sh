#!/bin/bash

#######################################
# generate the daily report and send it
# by mail
######################################
source $(dirname $0)/parameters.sh
find /var/log/archi -name '*.log' -mtime +6 -daystart | xargs rm -v \;
sudo find /var/log/httpd -name '*.gz' -mtime +6 -daystart | sudo xargs rm -v \;
sudo find /var/log/shibboleth -name '*.log' -mtime +6 -daystart | sudo xargs rm -v \;


echo "Purge $HOSTNAME" | mutt -s "Purge $HOSTNAME" $RECIPIENT 