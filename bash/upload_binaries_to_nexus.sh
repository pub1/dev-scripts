#!/bin/bash
if [[ $USER != "archi" ]]; then
    >&2 echo "this script must be run as archi user"
    exit 1
fi




echo "Packaging version : " $3
echo "using account " $1
USER=$1
PASSWORD=$2
VERSION=$3

BIN_REPO="https://repo/raw"


function upload() {
	echo "size of file to upload"
	du -h  $1
	echo "uploading to $BIN_REPO"
	curl -v --user $USER:$PASSWORD  --upload-file $1  $BIN_REPO/$1
	rm $1

}

##########################################
# collect binaries, tar them and upload to devin depot 
##########################################
# home dir
echo "[BUILDING] : binaries in home.dir/.archi/"
cd ~
tar -czf .archi-$VERSION.tar.gz .archi/
upload .archi-$VERSION.tar.gz

echo "end"