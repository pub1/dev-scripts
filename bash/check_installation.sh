#!/bin/bash
if [[ $USER != "archi" ]]; then
    >&2 echo "ce script doit etre execute en tant qu'archi"
    exit 1
fi



if [ $# -ne 1 ]; then
    >&2 echo "vous devez specifier un environnement <dev/rec/prd>"
    exit 1
fi


RED='\033[0;31m'
ORA='\033[38;5;202m'
GREEN="\033[32m"
YEL='\033[1;33m'
NC='\033[0m' # No Color

echo "#################################################"
echo " Begin installation tests for " $1
echo "#################################################"

USERAAA=$2
PASSWORD=$3

function printKo {
	if [ "$3" = "optional" ]
	then
		echo -e "${ORA}[KO]$1 $2${NC}"
	else
		echo -e "${RED}[KO]$1 $2${NC}"

	fi
}

function printOk {
        echo -e "${GREEN}[OK]$1 $2${NC}"
}


function checkIsRunning {
	if ! pidof $1 > /dev/null 
	then
		printKo $1  "process is not running"
	else
		printOk $1 "process is running"
	fi
}


function checkVariableValue {
	if [ "$1" = "$2" ] 
	then
		printOk $1 "got expected value"
	else
		printKo $1 "expected got $2 instead" $3
	fi
}

function checkFileExists {
	if [ -f "$1" ] 
	then
		printOk "$1" "file exists"
	else
		printKo "$1" "file not found"
	fi

}
function checkFolderExists {
	if [ -d "$1" ] 
	then
		printOk "$1" "folder exists"

	else
		printKo "$1" "folder not found"
	fi

}
function checkUserExists {
	if  id "$1" > /dev/null 
	then
		printOk "$1" "user exists"
	else
		printKo "$1" "user not found"
	fi

}
function checkGroupExists {
	if [ $(getent group "$1") ]
	then
		printOk "$1" "group exists"
	else
		printKo "$1" "group not found"
	fi
}
function checkYumInstalled {
  if yum list installed "$1" >/dev/null 2>&1; then
    printOk "$1" "package is installed"
  else
    printKo "$1" "package not found"
  fi
}
function checkFileContains {
	COUNT=$3
	if [ -z $3 ]
	then
		COUNT=1
	fi
	FOUND=$(grep -o "$1" $2 | wc -l)
	if [ "$FOUND" = "$COUNT" ]
	then
		printOk "$1" "was found in $2 ($COUNT match(es))"
	else
		if [ "$FOUND" = 0 ] 
		then
			printKo "$1" "not found in $2 "
		else
			printKo "$1" "found $FOUND matches in $2 but $COUNT match(es) expected"
		fi
	fi

}

echo "----------------------------------------------------------------"
echo " Phase 1 : user creations, vg  and fs, httpd service"
echo " everything should be OK"
echo "----------------------------------------------------------------"
checkFolderExists "/applis/archi"
checkFolderExists "/tmp/SERVEUR_PUBLI"
checkUserExists "archi"
checkGroupExists "archigrp"
checkVariableValue "$(sudo systemctl is-active httpd)" "active"


echo "----------------------------------------------------------------"
echo " Phase 2 : folder creations, archi binaries, fonts, muttrc, crontab"
echo "----------------------------------------------------------------"
echo "testing if folders exist on FS"
checkFolderExists "/applis/data/archi"
checkFolderExists "/applis/data/archi/history"
checkFolderExists "/applis/archi/noumea/.secrets"
checkFolderExists "/var/log/archi"
checkFolderExists "/var/www/html/archi"

checkFolderExists "/var/www/html/carto_fonctionnelle"
checkFolderExists "/usr/share/fonts/segoe"

echo "testing folders created from binaries"
checkFolderExists "/applis/archi/Archi"
checkFolderExists "/applis/archi/noumea"
checkFolderExists "/home/archi/.archi"


LAUNCH_ARCHI=$(/applis/archi/Archi/Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --help  | grep -o modelrepository | wc -l)
LAUNCH_ARCHI_WITH_JARCHI=$(/applis/archi/Archi/Archi -application com.archimatetool.commandline.app -consoleLog -nosplash --help | grep "script.runScript" | wc -l)

echo "testing if archi headless can be launched and has coArchi"
checkVariableValue "$LAUNCH_ARCHI" "10"
echo "testing if jArchi is deployed (not mandatory)"
checkVariableValue "$LAUNCH_ARCHI_WITH_JARCHI" "1" "optional"


checkFileExists "/var/www/html/index.html"
checkFolderExists "/var/www/html/assets"
echo "service httpd running ?"
checkVariableValue "$(sudo systemctl is-active httpd)" "active"

checkFileExists "/applis/archi/noumea/parameters.sh"
checkFileExists "/home/archi/.muttrc"

echo "Test if yum packages are installed"
checkYumInstalled "mutt"
checkYumInstalled "gtk3"
checkYumInstalled "xorg-x11-xauth"
checkYumInstalled "mesa-libGL"
checkYumInstalled "xorg-x11-server-Xvfb"
echo "Testing crontab (must have 4 jobs or 5 if history is used) "
checkVariableValue "$(sudo crontab -l -u archi | grep -o /applis/archi/noumea/ | wc -l)" "4" "optional"
checkVariableValue "$(sudo crontab -l -u archi | grep -o /applis/archi/noumea/ | wc -l)" "5" "optional"



echo "----------------------------------------------------------------"
echo " Phase 3 : TLS "
echo "----------------------------------------------------------------"
checkFileExists "/etc/httpd/conf.d/ssl.conf"
checkFileExists "/etc/httpd/conf.d/archi_httpd.conf"
checkFileExists "/etc/pki/tls/certs/localhost.cer"
checkFileExists "/etc/pki/tls/private/localhost.key"
checkFileExists "/applis/archi/noumea/.secrets/.htpasswd"

echo "----------------------------------------------------------------"
echo " Phase 4 : Shibboleth"
echo "----------------------------------------------------------------"
checkFileExists "/etc/yum.repos.d/shibboleth.repo"
checkFileExists "/etc/shibboleth/shibboleth2.xml"
checkFileExists "/etc/shibboleth/attribute-map.xml"
checkFileExists "/etc/httpd/conf.d/shib.conf"



checkFileContains "roles" "/etc/shibboleth/attribute-map.xml" "2"
checkFileContains "require shib-attr roles NOUMEA_PRIVE" "/etc/httpd/conf.d/shib.conf" "2"

echo "----------------------------------------------------------------"
echo " Phase 5 : post install"
echo "----------------------------------------------------------------"

echo "service shibd running ?"
checkVariableValue "$(sudo systemctl is-active shibd)" "active"
echo "service iptables running ?"
checkVariableValue "$(sudo systemctl is-active iptables)" "active"


checkIsRunning "httpd"

RETURN_CODE_HTTP=$(curl  -o  /dev/null -s -w '%{http_code}\n' http://localhost)
#ignore cert check on https (-k flag)
RETURN_CODE_HTTPS=$(curl -k -o  /dev/null -s -w '%{http_code}\n' https://localhost)

checkVariableValue "$RETURN_CODE_HTTP" "301"
checkVariableValue "$RETURN_CODE_HTTPS" "200"

TEST_SITE=$(curl -k -o  /dev/null -s -w '%{http_code}\n' https://localhost/test/index.html)
echo "testing if test site is deployed"
checkVariableValue "$TEST_SITE" "200"
TEST_SITE_PRIV=$(curl -k -o  /dev/null -s -w '%{http_code}\n' https://localhost/test_priv/index.html)
echo "testing if test_priv site  is deployed"
checkVariableValue "$TEST_SITE_PRIV" "200"


echo "end"
