#!/bin/bash
################################################
# sample script that tries to config iptables getting some
# of the rules from various config files (yum.repos.d, muttrc ...)
#################################################

echo "#####################################"
echo " purge everything"
echo "#####################################"
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -t nat -F
iptables -t mangle -F
iptables -F
iptables -X

ip6tables -P INPUT ACCEPT
ip6tables -P FORWARD ACCEPT
ip6tables -P OUTPUT ACCEPT
ip6tables -t nat -F
ip6tables -t mangle -F
ip6tables -F
ip6tables -X
echo "#####################################"
echo "adding input rules"
echo "#####################################"

echo "accept all responses to establish connections (ie make the firewall stateful)"
iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
echo "accepts http, ssh"
iptables -A INPUT -p tcp --dport 443 -j ACCEPT 
iptables -A INPUT -p tcp --dport 22 -j ACCEPT
echo "accepts any on  local interface"
iptables -I INPUT 2 -i lo -j ACCEPT
echo "allow pings"
iptables -A INPUT -p icmp -j ACCEPT



echo "#####################################"
echo "adding output rules"
echo "#####################################"

iptables -A OUTPUT -p all -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
echo "allow pings"
iptables -A OUTPUT -p icmp -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
echo "allow dns"
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT

echo "allow https to devin-source and devin-depot"
iptables -A OUTPUT -p tcp -d devin-source.rte-france.com --dport 443 -j ACCEPT
iptables -A OUTPUT -p tcp -d devin-depot.rte-france.com --dport 443 -j ACCEPT
echo "allow https to enabled yum repo"
#export the variable to pass it to awk
export REPO_RULE='iptables -A OUTPUT -p tcp --dport 443 -j ACCEPT -d '
# list enabled repos, grep their url (assuming https only), use sort to deduplicate  the result of grep then use awk to print the rule
sudo yum -v repolist enabled | grep -o "https:\/\/[^\/]*" | sort -u | awk '{ print ENVIRON["REPO_RULE"] substr($0,9, length($0)-1)}' | sh
echo "allow 3128 to proxy_surf : requirement for shibboleth install"
iptables -A OUTPUT -p tcp -d proxy-surf --dport 3128 -j ACCEPT

#ntp server from chrony config
echo "allow ntp udp and tcp from chrony configuration"
export NTP_RULE_TCP='iptables -A OUTPUT -p tcp --dport 123 -j ACCEPT -d '
export NTP_RULE_UDP='iptables -A OUTPUT -p udp --dport 123 -j ACCEPT -d '
grep "^server" /etc/chrony.conf | awk {'print ENVIRON["NTP_RULE_TCP"] $2'} | sh
grep "^server" /etc/chrony.conf | awk {'print ENVIRON["NTP_RULE_UDP"] $2'} | sh
# TODO not sure if it a bug but chronyc does not work when ran by a non root users
echo "add smtp out from archi user .muttrc file" 
export SMTP_RULE='iptables -A OUTPUT -p tcp --dport 25 -j ACCEPT -d ' 
# works but add two rules, one for each of karlito adress
grep -o "smtp:/[^']*" /home/archi/.muttrc | awk {'print ENVIRON["SMTP_RULE"] substr($0,8, length($0)-1)'} | sh


echo " #####################################"
echo " default rules, drop everything else"
echo " #####################################"
 
iptables -A INPUT -j DROP
iptables -A FORWARD -j DROP
iptables -A OUTPUT -j DROP
echo "---------------------------------------------------------------------"
echo "please check result before saving"
iptables -vL
