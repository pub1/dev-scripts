' #############################################################
' # VBS script code taken from various VBA sources 
' # Generate an excel file with all comments found in every docx file in a given folder
' # One excel sheet per docx with, page, paragraph, text, comment, author
' # use it with a bat file 
' #     cscript "list_comments.vbs" "FOLDER_PATH"
' #     pause
' # THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
' #############################################################

' #############################################################
' #
' # Define Objects and Variables
' # 

Dim oFSO
Dim LogFile
Dim strPath
Dim strFileName
Dim strLogFile
Dim strInputFile
Dim strOutputFile
Dim objWord
Dim objExcel
Dim objExcelSheet
Dim intCommentCount
Dim x

Const wdDoNotSaveChanges = 0

Set objFSO = CreateObject("Scripting.FileSystemObject")
Set stdout = objFSO.GetStandardStream (1)


if WScript.Arguments.Count = 0 then
    WScript.Echo "Préciser le chemin"
end if


strFileName = "comments"
strPath = WScript.Arguments(0) & "\"

strInputFile = strPath & strFileName & ".docx"
strOutputFile = strPath & strFileName & ".xlsx"
stdout.WriteLine strOutputFile
Set objFolder = objFSO.GetFolder(strPath)

'On Error resume Next




' #############################################################
' #
' # Setup
' # 

' Set up log file
Set oFSO = CreateObject("Scripting.FileSystemObject")



' Open new Excel spreadsheet for output

Set objExcel = CreateObject("Excel.Application")
objExcel.Visible = false


Set objWorkbook = objExcel.Workbooks.Add(1)

' loop on each docx file found in folder
Set allFiles = objFolder.Files
For Each objFile in allFiles
    If UCase(objFSO.GetExtensionName(objFile.name)) = "DOCX" Then
        If Left(objFile.Name,1)<>"~" then
            stdout.WriteLine objFile.Name
            processFile objFile, objExcel
        end If
        
    End if
Next


sub processFile(objFile, objWorkbook)

    Set objWord = CreateObject("Word.Application")
    objWord.Visible = false
    objWord.documents.open(objFile.Path)
    
    ' #############################################################
    ' #
    ' # Start work
    ' # 

    ' How many comments are found?
    intCommentCount = objWord.Documents(1).Comments.Count

    if intCommentCount >= 1 then

        ' create a sheet for current word file
        Set objWorkSheet = objWorkbook.Sheets.Add 
        objWorkSheet.Name =  Mid(objFile.Name,1,30)
        ' Set output columns
        objWorkSheet.Cells( 1, 1 ).Value = "Page"
        objWorkSheet.Cells( 1, 2 ).Value = "Paragraphe"
        objWorkSheet.Cells( 1, 3 ).Value = "Texte"
        objWorkSheet.Cells( 1, 4 ).Value = "Commentaire"
        objWorkSheet.Cells( 1, 5 ).Value = "Auteur"
        

        ' Iterate over comments
        for x = 1 to intCommentCount
            objWorkSheet.Cells( x+1, 1 ).Value = objWord.Documents(1).Comments(x).Scope.Information(3) '3 = wdActiveEndPageNumber
            objWorkSheet.Cells( x+1, 2 ).Value = parGetNum(objWord.Documents(1).Comments(x))
            objWorkSheet.Cells( x+1, 3 ).Value = objWord.Documents(1).Comments(x).Scope
            objWorkSheet.Cells( x+1, 4 ).Value = objWord.Documents(1).Comments(x).Range
            objWorkSheet.Cells( x+1, 5 ).Value = objWord.Documents(1).Comments(x).Author
        next

    end if


    objWord.documents.close(wdDoNotSaveChanges)
    objWord.Quit

    Set objWord = Nothing

    
    
    
    
end sub

' return paragraph number
Function parGetNum(COMM) 

    
    'Set heading = COMM.Reference.GoTo(wdGoToHeading, wdGoToPrevious)
    Set heading = COMM.Reference.GoTo(11, 3)
    
    ' Get heading numbering string
    hNum = heading.ListFormat.ListString
    
    ' Get heading text
    
    ' Expand the range to the whole paragraph (final CR included)
    
    heading.Expand 3 '3 correspond à wdUnits.wdSentence mais je ne comprends pas pourquoi il faut lui préciser la valeur numérique
    hText = heading.Text
    
    parGetNum = hNum & vbTab & """" & Left(hText, Len(hText) - 1) & """"


End Function

' #############################################################
' #
' # Teardown
' # 

' Close Excel spreadsheet
objExcel.ActiveWorkbook.SaveAs(strOutputFile)
objExcel.Quit
Set objExcel = Nothing




' Tear down log file
Set LogFile = Nothing
Set oFSO = Nothing

wscript.echo("Finished!")
