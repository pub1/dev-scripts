# Dev Bash

Some sample scripts I have written :
- place_content_in_files_with_sed.sh : how to modify html code in a set of files, with values taken from another set of files
- installation_app : misc installation steps (add crontab, untar, ...)
- upload_binaries_to_nexus : tar, use of functions
- purge_logs : as it sounds...
- colors : colorized messages in terminal, include another script
- check_installation.sh : very basic functions to test if everything is ok after deploying an application on a new server